# Memory types 

## Keyword dictionary ENG-PL

| Keyword | Translation |
| ----- | ----- |
| Solid state memory | pamięć półprzewodnikowa |


#### Nonvolatile storage

There are two primary nonvolatile starage technologies:
- solid state memory the most prevalent for embedded systems  
- magnetic storage media (form of hard drivers)

Modern `solid state memory` is called `Flash memory`

Features:
- it can be erased and reprogrammed by a software driver 
- Read speed is relatively fast (slower than DRAM), but faster then Hard Disk)
- Write time are slower than read time

`There are two distinct flash memory devices types: `

| NOR Flash | NAND Flash |
| ----- | ----- |
| Lower density | Much higher density |
| severeal MB of storage | up to gigabytes storage | 
| faster to read | slower to read |
| slower to erase & erase new data | faster | 
| larger cell size | smaller |
| more expensive | cheaper | 

#### Memory cells organisation
![NOR_Schema](pics/memory/NOR_Schema.png)  
*1. NOR Flash memory* 

```
One end of each memory cell is connected to source line 
And the other to a bit line reasembling NOR gate 
```

![NOR_Schema](pics/memory/NAND_Schema.png)  
*2. NAND Flash memory* 

```
Several memory cells connected in a paraller series    
And the other to a bit line reasembling NOR gate  
```

